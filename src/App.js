import logo from "./logo.svg";
import "./App.css";
import ListCar from "./Listcar/ListCar";
import Dropzone from "react-dropzone-uploader";
import "react-dropzone-uploader/dist/styles.css";
import { useState, useEffect } from "react";
import { Bar } from "react-chartjs-2";
//eslint-disable-next-line
import { Chart } from "chart.js/auto";

function App() {
  const [files, setFiles] = useState();
  const [dataCar, setDataCar] = useState();

  const getDataCar = async () => {
    const data = await fetch(`https://rent-cars-api.herokuapp.com/admin/car
    `);
    const result = await data.json();
    setDataCar(result);
  };

  const getUploadParams = ({ meta }) => {
    setFiles(meta);
    return { url: "https://httpbin.org/post" };
  };

  const handleChangeStatus = ({ meta, file }, status) => {
    console.log(`Perubahan terjadi ${status}`);
    setFiles(meta);
  };
  const handleSubmit = (files, allFiles) => {
    console.log(`Files => ${files} && all FIles => ${allFiles}`);
    allFiles.forEach((f) => f.remove());
  };

  useEffect(() => {
    getDataCar();
  }, [files]);

  const dataChart = {
    labels: ["January", "February", "March"],
    datasets: [
      {
        label: "Penjualan",
        data: dataCar?.map((item) => item.price),
        backgroundColor: "royalblue",
      },
      {
        label: "Belum laku",
        data: [20, 10, 100],
        backgroundColor: "salmon",
      },
    ],
  };

  return (
    <>
      <div style={{ width: "50%" }}>
        <Bar data={dataChart} redraw={true} />
      </div>
      <Dropzone getUploadParams={getUploadParams} onChangeStatus={handleChangeStatus} onSubmit={handleSubmit} accept="image/*, audio/*, video/*" />
      <ListCar />
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
            Learn React
          </a>
        </header>
      </div>
    </>
  );
}

export default App;
